-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2018 at 02:59 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kopiadem_belajar`
--

-- --------------------------------------------------------

--
-- Table structure for table `daftar_gayabelajar`
--

CREATE TABLE `daftar_gayabelajar` (
  `kode_gayabelajar` varchar(3) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `nama_gayabelajar` varchar(40) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daftar_gayabelajar`
--

INSERT INTO `daftar_gayabelajar` (`kode_gayabelajar`, `gambar`, `nama_gayabelajar`, `deskripsi`) VALUES
('T1', 'images/gayabelajar/visual.jpg', 'VISUAL', '<span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"> 			Gaya Belajar Visual (Visual Learners) menitik beratkan pada ketajaman penglihatan. Artinya, bukti-bukti konkret harus diperlihatkan terlebih dahulu agar mereka paham. Gaya belajar seperti ini mengandalkan penglihatan atau melihat dulu buktinya untuk kemudian bisa memercayainya. Ada beberapa karakteristik yang khas bagi orang-orang yang menyukai gaya belajar visual ini. &nbsp; 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">Pertama adalah kebutuhan melihat sesuatu (informasi atau pelajaran) secara visual untuk mengetahuinya atau memahaminya</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">kedua memiliki kepekaan yang kuat terhadap warna</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">ketiga memiliki pemahaman yang cukup terhadap masalah artistik</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">keempat memiliki kesulitan dalam berdialog secara langsung</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">kelima terlalu reaktif terhadap suara</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">keenam sulit mengikuti anjuran secara lisan</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">ketujuh seringkali salah menginterpretasikan kata atau ucapan.</span></li> 	</ul> 	<div style=\"text-align: justify; line-height: 2;\"> 		<span style=\"font-family: Arial;\"> 		<br> 		<b>Melatih dan Mengembangkan Potensi Pembelajar Visual</b> 		<br> 		Berikut beberapa cara yang dapat dilakukan untuk melatih dan mengembangkan potensi anak yang belajar dengan tipe visual: 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">1. Gunakan Media Belajar yang Menarik</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">2. Pertahankan Kontak Mata</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">3. Berikan Petunjuk Tertulis</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">4. Gunakan Sesi Tanya Jawab</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">5. Libatkan dalam Diskusi Kelompok</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">6. Menulis atau Membuat Formulasi</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">7. Ciptakan Suasana yang Tenang</span></li> 	</ul> <div style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"></span></div> <div style=\"line-height: 2;\"><br></div>'),
('T2', 'images/gayabelajar/auditori.jpg', 'AUDITORI', '<span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"> 			Gaya Belajar Auditori (Auditory Learners) mengandalkan pada pendengaran untuk bisa memahami dan mengingatnya. Karakteristik model belajar seperti ini benar-benar menempatkan pendengaran sebagai alat utama menyerap informasi atau pengetahuan. Artinya, harus mendengar, baru kemudian bisa mengingat dan memahami informasi. Karakter pertama orang yang memiliki gaya belajar auditori adalah semua informasi hanya bisa diserap melalui pendengaran, kedua memiliki kesulitan untuk menyerap informasi dalam bentuk tulisan secara langsung, ketiga memiliki kesulitan menulis ataupun membaca.&nbsp; 		</span> 	</div> 	<div style=\"text-align: justify; line-height: 2;\"> 		<span style=\"font-family: Arial;\"> 		<br> 		<b>Melatih dan Mengembangkan Potensi Pembelajar Auditori</b> 		<br> 		Berikut beberapa cara yang dapat dilakukan untuk melatih dan mengembangkan potensi anak yang belajar dengan tipe auditori: 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">1. Gunakan Media Berbentuk Cerita</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">2. Perjelas Intonasi dan Tekanan Suara</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">3. Berikan Petunjuk Secara Lisan</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">4. Berikan Tugas Presentasi</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">5. Bentuk Diskusi Kelompok</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">6. Latih Kemampuan Menulisnya</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">7. Ciptakan Suasana yang Interaktif</span></li> 	</ul> <div style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"></span></div> <div style=\"line-height: 2;\"><br></div>'),
('T3', 'images/gayabelajar/kinestetik.jpg', 'KINESTETIK', '<span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"> 			Gaya Belajar Kinestetik (Kinesthetic Learners) mengharuskan individu yang bersangkutan menyentuh sesuatu yang memberikan informasi tertentu agar ia bisa mengingatnya. Tentu saja ada beberapa karakteristik model belajar seperti ini yang tak semua orang bisa melakukannya. Karakter pertama adalah menempatkan tangan sebagai alat penerima informasi utama agar bisa terus mengingatnya. Hanya dengan memegangnya saja, seseorang yang memiliki gaya belajar kinestetik bisa menyerap informasi tanpa harus membaca penjelasannya.&nbsp; 		</span> 	</div> 	<div style=\"text-align: justify; line-height: 2;\"> 		<span style=\"font-family: Arial;\"> 		<br> 		<b>Melatih dan Mengembangkan Potensi Pembelajar Kinestetik</b> 		<br> 		Berikut beberapa cara yang dapat dilakukan untuk melatih dan mengembangkan potensi anak yang belajar dengan tipe kinestetik: 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">1. Gunakan Alat Bantu Peraga</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">2. Ciptakan Permainan Edukasi</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">3. Lakukan Kegiatan Praktikum</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">4. Lakukan Aktivitas di Luar Kelas</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">5. Latih Kemampuan Menulisnya</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">6. Berikan Aktivitas Ektsrakurikuler</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">7. Ciptakan Suasana Belajar yang Aplikatif</span></li> 	</ul> <div style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"></span></div> <div style=\"line-height: 2;\"><br></div>');

-- --------------------------------------------------------

--
-- Table structure for table `rule`
--

CREATE TABLE `rule` (
  `idpertanyaan` int(11) NOT NULL,
  `solusi_dan_pertanyaan` text NOT NULL,
  `bila_benar` int(11) NOT NULL,
  `bila_salah` int(11) NOT NULL,
  `mulai` varchar(1) NOT NULL,
  `selesai` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rule`
--

INSERT INTO `rule` (`idpertanyaan`, `solusi_dan_pertanyaan`, `bila_benar`, `bila_salah`, `mulai`, `selesai`) VALUES
(1, '<h2><center>Apakah anda cenderung melihat sikap, gerakan, dan bibir pengajar yang sedang mengajar?</h2></center>', 2, 8, 'Y', 'N'),
(2, '<h2><center>Apakah anda bukan pendengar yang baik saat berkomunikasi?</h2></center>', 3, 3, 'Y', 'N'),
(3, '<h2><center>Apakah anda saat mendapat petunjuk untuk melakukan sesuatu, anda akan melihat orang sekitar terlebih dahulu kemudian anda baru bertindak?</h2></center>', 4, 4, 'Y', 'N'),
(4, '<h2><center>Apakah anda tak suka bicara didepan kelompok dan tak suka mendengarkan orang lain?</h2></center>', 5, 5, 'Y', 'N'),
(5, '<h2><center>Apakah anda kurang mampu mengingat informasi yang diberikan secara lisan?</h2></center>', 6, 6, 'Y', 'N'),
(6, '<h2><center>Apakah anda lebih suka peragaan daripada penjelasan lisan?</h2></center>', 7, 8, 'Y', 'N'),
(7, '<h2><center>Apakah anda dapat duduk tenang ditengah situasi yang ribut dan ramai tanpa terganggu?</h2></center>', 22, 8, 'Y', 'N'),
(8, '<h2><center>Apakah anda mampu mengingat dengan baik penjelasan pengajar di depan kelas, atau materi yang didiskusikan dalam kelompok atau kelas?</h2></center>', 9, 15, 'Y', 'N'),
(9, '<h2><center>Apakah anda pendengar ulung: mudah menguasai materi iklan atau lagu di televisi atau radio?</h2></center>', 10, 10, 'Y', 'N'),
(10, '<h2><center>Apakah anda cenderung banyak omong?</h2></center>', 11, 11, 'Y', 'N'),
(11, '<h2><center>Apakah anda tak suka membaca dan kurang dapat mengingat dengan baik apa yang baru saja dibaca?</h2></center>', 12, 12, 'Y', 'N'),
(12, '<h2><center>Apakah anda kurang cakap dalam mengerjakan tugas mengarang atau menulis?</h2></center>', 13, 13, 'Y', 'N'),
(13, '<h2><center>Apakah anda senang berdiskusi dan berkomunikasi dengan orang lain?</h2></center>', 14, 15, 'Y', 'N'),
(14, '<h2><center>Apakah anda kurang tertarik memerhatikan hal-hal baru dilingkungan sekitar?</h2></center>', 23, 15, 'Y', 'N'),
(15, '<h2><center>Apakah anda menyentuh segala sesuatu yang dijumpai, termasuk saat belajar?</h2></center>', 16, 25, 'Y', 'N'),
(16, '<h2><center>Apakah anda sulit berdiam diri atau duduk manis, dan selalu ingin bergerak?</h2></center>', 17, 17, 'Y', 'N'),
(17, '<h2><center>Apakah anda mengerjakan segala sesuatu yang memungkinkan tangan anda aktif? (misal: saat pengajar menerangkan pelajaran, anda mendengarkan sambil tangan anda asyik menggambar)</h2></center>', 18, 18, 'Y', 'N'),
(18, '<h2><center>Apakah anda suka menggunakan objek nyata sebagai alat bantu belajar?</h2></center>', 19, 19, 'Y', 'N'),
(19, '<h2><center>Apakah anda sulit menguasai hal-hal abstrak seperti peta, symbol dan lambing?</h2></center>', 20, 20, 'Y', 'N'),
(20, '<h2><center>Apakah anda menyukai praktek atau percobaan?</h2></center>', 21, 25, 'Y', 'N'),
(21, '<h2><center>Apakah anda menyukai permainan dan aktivitas fisik?</h2></center>', 24, 25, '', ''),
(22, '<h3><span style=\"font-family: Arial; font-weight: bold;\">VISUAL (Visual Learners)</span></h3> 	<div style=\"text-align: justify; line-height: 2;\"><div class=\"col-md-12\"><center><img src=\"images/gayabelajar/visual.jpg\" alt=\"\" class=\"img-responsive\"></div></center> 		<span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"> 			Gaya Belajar Visual (Visual Learners) menitik beratkan pada ketajaman penglihatan. Artinya, bukti-bukti konkret harus diperlihatkan terlebih dahulu agar mereka paham. Gaya belajar seperti ini mengandalkan penglihatan atau melihat dulu buktinya untuk kemudian bisa memercayainya. Ada beberapa karakteristik yang khas bagi orang-orang yang menyukai gaya belajar visual ini. &nbsp; 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">Pertama adalah kebutuhan melihat sesuatu (informasi atau pelajaran) secara visual untuk mengetahuinya atau memahaminya</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">kedua memiliki kepekaan yang kuat terhadap warna</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">ketiga memiliki pemahaman yang cukup terhadap masalah artistik</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">keempat memiliki kesulitan dalam berdialog secara langsung</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">kelima terlalu reaktif terhadap suara</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">keenam sulit mengikuti anjuran secara lisan</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">ketujuh seringkali salah menginterpretasikan kata atau ucapan.</span></li> 	</ul> 	<div style=\"text-align: justify; line-height: 2;\"> 		<span style=\"font-family: Arial;\"> 		<br> 		<b>Melatih dan Mengembangkan Potensi Pembelajar Visual</b> 		<br> 		Berikut beberapa cara yang dapat dilakukan untuk melatih dan mengembangkan potensi anak yang belajar dengan tipe visual: 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">1. Gunakan Media Belajar yang Menarik</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">2. Pertahankan Kontak Mata</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">3. Berikan Petunjuk Tertulis</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">4. Gunakan Sesi Tanya Jawab</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">5. Libatkan dalam Diskusi Kelompok</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">6. Menulis atau Membuat Formulasi</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">7. Ciptakan Suasana yang Tenang</span></li> 	</ul> <div style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"></span></div> <div style=\"line-height: 2;\"><br></div>', 0, 0, 'N', 'Y'),
(23, '<h3><span style=\"font-family: Arial; font-weight: bold;\">AUDITORI (Auditory Learners)</span></h3> 	<div style=\"text-align: justify; line-height: 2;\"><div class=\"col-md-12\"><center><img src=\"images/gayabelajar/auditori.jpg\" alt=\"\" class=\"img-responsive\"></div></center> 		<span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"> 			Gaya Belajar Auditori (Auditory Learners) mengandalkan pada pendengaran untuk bisa memahami dan mengingatnya. Karakteristik model belajar seperti ini benar-benar menempatkan pendengaran sebagai alat utama menyerap informasi atau pengetahuan. Artinya, harus mendengar, baru kemudian bisa mengingat dan memahami informasi. Karakter pertama orang yang memiliki gaya belajar auditori adalah semua informasi hanya bisa diserap melalui pendengaran, kedua memiliki kesulitan untuk menyerap informasi dalam bentuk tulisan secara langsung, ketiga memiliki kesulitan menulis ataupun membaca.&nbsp; 		</span> 	</div> 	<div style=\"text-align: justify; line-height: 2;\"> 		<span style=\"font-family: Arial;\"> 		<br> 		<b>Melatih dan Mengembangkan Potensi Pembelajar Auditori</b> 		<br> 		Berikut beberapa cara yang dapat dilakukan untuk melatih dan mengembangkan potensi anak yang belajar dengan tipe auditori: 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">1. Gunakan Media Berbentuk Cerita</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">2. Perjelas Intonasi dan Tekanan Suara</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">3. Berikan Petunjuk Secara Lisan</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">4. Berikan Tugas Presentasi</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">5. Bentuk Diskusi Kelompok</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">6. Latih Kemampuan Menulisnya</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">7. Ciptakan Suasana yang Interaktif</span></li> 	</ul> <div style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"></span></div> <div style=\"line-height: 2;\"><br></div>', 0, 0, 'N', 'Y'),
(24, '<h3><span style=\"font-family: Arial; font-weight: bold;\">KINESTETIK (Kinesthetic Learners)</span></h3> 	<div style=\"text-align: justify; line-height: 2;\"><div class=\"col-md-12\"><center><img src=\"images/gayabelajar/kinestetik.jpg\" alt=\"\" class=\"img-responsive\"></div></center> 		<span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"> 			Gaya Belajar Kinestetik (Kinesthetic Learners) mengharuskan individu yang bersangkutan menyentuh sesuatu yang memberikan informasi tertentu agar ia bisa mengingatnya. Tentu saja ada beberapa karakteristik model belajar seperti ini yang tak semua orang bisa melakukannya. Karakter pertama adalah menempatkan tangan sebagai alat penerima informasi utama agar bisa terus mengingatnya. Hanya dengan memegangnya saja, seseorang yang memiliki gaya belajar kinestetik bisa menyerap informasi tanpa harus membaca penjelasannya.&nbsp; 		</span> 	</div> 	<div style=\"text-align: justify; line-height: 2;\"> 		<span style=\"font-family: Arial;\"> 		<br> 		<b>Melatih dan Mengembangkan Potensi Pembelajar Kinestetik</b> 		<br> 		Berikut beberapa cara yang dapat dilakukan untuk melatih dan mengembangkan potensi anak yang belajar dengan tipe kinestetik: 		</span> 	</div> 	<ul> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">1. Gunakan Alat Bantu Peraga</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">2. Ciptakan Permainan Edukasi</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">3. Lakukan Kegiatan Praktikum</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">4. Lakukan Aktivitas di Luar Kelas</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">5. Latih Kemampuan Menulisnya</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">6. Berikan Aktivitas Ektsrakurikuler</span></li> 		<li style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\">7. Ciptakan Suasana Belajar yang Aplikatif</span></li> 	</ul> <div style=\"line-height: 2;\"><span style=\"background-color: rgb(255, 255, 255); font-family: Arial;\"></span></div> <div style=\"line-height: 2;\"><br></div>', 0, 0, 'N', 'Y'),
(25, '<br><br><h2><center> Tipe Gaya Belajar yang Anda Masukan tidak dapat Ditemukan</center></h2> <h2><center><br></center></h2> <h2><center><br></center></h2> <input type=\"submit\" class=\"btn btn-success btn-block btn-large\" value=\"Kembali melakukan konsultasi\">', 0, 0, 'N', 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `daftar_gayabelajar`
--
ALTER TABLE `daftar_gayabelajar`
  ADD PRIMARY KEY (`kode_gayabelajar`);

--
-- Indexes for table `rule`
--
ALTER TABLE `rule`
  ADD PRIMARY KEY (`idpertanyaan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
