<?php include ('conf/conn.php'); ?>
<!DOCTYPE html>
<head>
	<title>Sistem Pakar Gaya Belajar</title>
	<!-- Meta-tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="Lively Ride Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>

	<!-- //Meta-tags -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />

	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/font-awesome.css" rel="stylesheet">
	<!--web-fonts-->
	<link href="//fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Monoton" rel="stylesheet">
	<!--//web-fonts-->
</head>

<body>
	<!-- banner -->
	<div class="banner" id="home">
		<!-- header -->
		<header>
			<div class="container">

				<!-- navigation -->
				<div class="w3_navigation">
					<nav class="navbar navbar-default">
						<div class="navbar-header navbar-left">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<div class="w3_navigation_pos">
								<h1>
									<a href="index.php">
										<span>S</span>istem
										<span>P</span>akar</a>
								</h1>
							</div>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
							<nav class="menu menu--miranda">
								<ul class="nav navbar-nav menu__list">
									<li class="menu__item">
										<a href="index.php" class="menu__link">Home</a>
									</li>
									<li class="menu__item">
										<a href="about.php" class=" menu__link">About</a>
									</li>
									<li class="menu__item">
										<a href="help.php" class=" menu__link">Help</a>
									</li>
									<li class="menu__item">
									</li>
									<li class="menu__item">
										<a href="konsultasi.php" class=" menu__link">Konsultasi</a>
									</li>
									<li class="menu__item menu__item--current">
										<a href="tipe_gaya_belajar.php" class=" menu__link">Tipe Gaya Belajar</a>
									</li>
								</ul>
							</nav>
						</div>
					</nav>
				</div>
				<div class="clearfix"></div>
				<!-- //navigation -->
			</div>
		</header>
		<!-- //header -->
		<br><br><br><br><br><br><br><br><br><br><br><br><br>
	<!-- //banner -->
<div class="container">
			<h2 class="tittle-w3">Tipe
				<span>Gaya Belajar</span>
			</h2>
			<?php 
            $query = $conn->prepare('SELECT * FROM daftar_gayabelajar');
            $query->execute();
            $result=$query->get_result();
            $jumrow=$result->num_rows;
        
            if($jumrow > 0){
            while ($row=$result->fetch_array()) {
                $id = $row['kode_gayabelajar'];    
                $gambar = $row['gambar'];    
                $nama_penyakit = $row['nama_gayabelajar'];    

                echo '
                    <div class="col-md-4">
                    <h2><a href="detail_tipe_gaya_belajar.php?id='.$id.'">'.$nama_penyakit.'</a></h2><br>
                    <a href="detail_tipe_gaya_belajar.php?id='.$id.'">
						<img border="0" alt="W3Schools" src="'.$gambar.'" width="280" height="280" class="img-responsive">
					</a>
                </div>
                ';
                }  
            }
            ?>		    
	</div>


	<!-- about-top -->
		
	</div>
	<!-- //about-top -->
	<!-- Footer -->
	<div class="copyright-wthree">
		<div class="container">
			<p>© 2018 Sistem Pakar Gaya Belajar | Design by <a href="izulmi.blogspot.co.id">who're you</a>. All Right Reserved</p>
		</div>
	</div>
	<!-- //Footer -->
	<a href="#home" class="scroll" id="toTop" style="display: block;">
		<span id="toTopHover" style="opacity: 1;"> </span>
	</a>
	<!-- //smooth scrolling -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<!-- Responsive-slider-->
	<script src="js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider").responsiveSlides({
				auto: true,
				pager: true,
				nav: false,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
	</script>

	<!--//  Responsive-slider -->
	<!-- stats -->
	<script src="js/jquery.waypoints.min.js"></script>
	<script src="js/jquery.countup.js"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});

		});
	</script>
	<!-- //here ends scrolling icon -->
	<!--js for bootstrap working-->
	<script src="js/bootstrap.js"></script>
	<!-- //for bootstrap working -->


	<!-- script-for-menu -->
	<script>
		$("span.menu").click(function () {
			$(".top-nav ul").slideToggle("slow", function () {});
		});
	</script>
	<!-- script-for-menu -->
</body>

</html>